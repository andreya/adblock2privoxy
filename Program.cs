﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;

namespace adblock2privoxy
{
    class Program
    {
        private delegate void DebugOutput(string format, params object[] parameters);

        private static int Main(string[] args)
        {
            #region Defaults

            var configPath = @"C:\Program Files (x86)\Privoxy\";
            string userBaseDir = ".";
            var userFileName = "adblock";
            var fixConfig = false;
            var reload = false;
            var sources = new List<string> { "https://easylist-downloads.adblockplus.org/ruadlist+easylist.txt", "https://raw.github.com/liamja/Prebake/master/obtrusive.txt" };
            DebugOutput debug = (f, p) => { };

            #endregion

            #region Command line parser

            var argList = args.SelectMany(x => x[0] == '-' && x[1] != '-' ? x.Substring(1).Select(z => "-" + z) : new[] { x });
            var argsList = new List<Tuple<string, IEnumerable<string>>>();

            List<string> current = null;
            foreach (var arg in argList)
            {
                if (arg.StartsWith("-") || arg.StartsWith("/") || arg.StartsWith("\\"))
                {
                    argsList.Add(new Tuple<string, IEnumerable<string>>(arg.TrimStart('-', '/', '\\'), current = new List<string>()));
                }
                else
                {
                    if (current == null)
                    {
                        Help("Unexpected argument", arg);
                        return -1;
                    }
                    current.Add(arg);
                }
            }
            foreach (var a in argsList)
            {
                switch (a.Item1.ToLower())
                {
                    case "c":
                    case "config":
                        {
                            if (a.Item2.Count() != 1)
                            {
                                Help("Expected exactly one argument after --config");
                                return -1;
                            }
                            configPath = a.Item2.First();
                            break;
                        }
                    case "s":
                    case "source":
                    case "sources":
                        {
                            if (!a.Item2.Any())
                                Help("Sources list cannot be empty");
                            sources = a.Item2.ToList();
                            break;
                        }
                    case "f":
                    case "file":
                    case "configfile":
                        {
                            if (a.Item2.Count() != 1)
                            {
                                Help("Expected exactly one argument after --file");
                                return -1;
                            }
                            userFileName = a.Item2.First();
                            break;
                        }
                    case "x":
                    case "fix":
                    case "fixconfig":
                        {
                            if (a.Item2.Any())
                            {
                                Help("Fix argument does not require any parameters");
                                return -1;
                            }
                            fixConfig = true;
                            break;
                        }
                    case "r":
                    case "reload":
                        {
                            if (a.Item2.Any())
                            {
                                Help("Reload argument does not require any parameters");
                                return -1;
                            }
                            reload = true;
                            break;
                        }
                    case "h":
                    case "help":
                    case "?":
                        {
                            Help(null);
                            return 0;
                        }
                    case "v":
                    case "verbose":
                        {
                            if (a.Item2.Any())
                            {
                                Help("Verbose argument does not require any parameters");
                                return -1;
                            }
                            debug = Console.Error.WriteLine;
                            break;
                        }
                    default:
                        Help("Unknown argument", a.Item1);
                        return -1;
                }
            }

            #endregion

            #region Read/fix privoxy's config.txt

            debug("Reading config.txt from {0}", Path.Combine(configPath, "config.txt"));
            if (!File.Exists(Path.Combine(configPath, "config.txt")))
            {
                Console.Error.WriteLine("Cannot find config.txt");
                return -2;
            }
            try
            {
                var hasActions = false;
                var hasFilters = false;
                using (var f = File.OpenText(Path.Combine(configPath, "config.txt")))
                {
                    string line;
                    while ((line = f.ReadLine()) != null)
                    {
                        var c = line.IndexOf('#');
                        if (c != -1)
                            line = line.Substring(0, c);
                        if (line.Length == 0)
                            continue;
                        var arg = line.Trim();
                        string val = null;
                        c = arg.IndexOfAny(new[] { ' ', '\t' });
                        if (c != -1)
                        {
                            val = arg.Substring(c + 1).Trim();
                            arg = arg.Substring(0, c);
                        }
                        switch (arg)
                        {
                            case "config":
                                userBaseDir = val;
                                break;
                            case "actionsfile":
                                hasActions |= val == userFileName + ".action";
                                break;
                            case "filterfile":
                                hasFilters |= val == userFileName + ".filter";
                                break;
                        }
                    }

                }
                if (!hasFilters || !hasActions)
                {
                    if (fixConfig)
                    {
                        debug("Fixing config.txt");
                        using (var f = new StreamWriter(Path.Combine(configPath, "config.txt"), true))
                        {
                            f.WriteLine();
                            if (!hasFilters)
                                f.WriteLine("filterfile {0}.filter", userFileName);
                            if (!hasActions)
                                f.WriteLine("actionsfile {0}.action", userFileName);
                        }
                    }
                    else
                    {
                        Console.Error.WriteLine("Warning: your config.txt does not contain reference to generated actions and/or filters file");
                    }
                }
            }
            catch (IOException e)
            {
                Console.Error.WriteLine("Error reading/writing config.txt: {0}", e.Message);
                return -2;
            }

            #endregion

            debug("Using user directory {0}", Path.Combine(configPath, userBaseDir));
            var sed = new ProcessStartInfo(Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), "sed.exe"));
            sed.RedirectStandardOutput = true;
            sed.UseShellExecute = false;
            using (var actions = File.CreateText(Path.Combine(configPath, userBaseDir, userFileName + ".action")))
            using (var filters = File.CreateText(Path.Combine(configPath, userBaseDir, userFileName + ".filter")))
            {
                foreach (var source in sources)
                {
                    var fileref = Path.GetFileNameWithoutExtension(source).Replace('+', '-');
                    debug("Downloading {0}", source);
                    var rq = WebRequest.Create(source);
                    var rs = rq.GetResponse();
                    var tmp = Path.GetTempFileName();
                    using (var temp = File.Create(tmp))
                        rs.GetResponseStream().CopyTo(temp);
                    debug("Writing blocklist");
                    sed.Arguments =
                        @"""/^!.*/d;1,1 d;/^@@.*/d;/\$.*/d;/#/d;s/\./\\./g;s/\\[\\]/\\\\[\\\\]/g;s/\?/\\?/g;s/\*/.*/g;s/(/\\(/g;s/)/\\)/g;s/\[/\\[/g;s/\]/\\]/g;s/\^/[\/\&:\?=_]/g;s/^||/\./g;s/^|/^/g;s/|$/\$/g;/|/d"" """ + tmp +
                        "\"";
                    var proc = Process.Start(sed);
                    using (var reader = proc.StandardOutput)
                    {
                        var lines = reader.ReadToEnd();
                        actions.WriteLine("{{ +block{{{0}}} }}", fileref);
                        actions.WriteLine(lines.Trim());
                    }
                    debug("Writing filters");
                    sed.Arguments =
                        @"""/^#/!d;s/^##//g;s/^#\(.*\)\[.*\]\[.*\]*/s|<([a-zA-Z0-9]+)\\s+.*id=.?\1.*>.*<\/\\1>||g/g;s/^#\(.*\)/s|<([a-zA-Z0-9]+)\\s+.*id=.?\1.*>.*<\/\\1>||g/g;s/^\.\(.*\)/s|<([a-zA-Z0-9]+)\\s+.*class=.?\1.*>.*<\/\\1>||g/g;s/^a\[\(.*\)\]/s|<a.*\1.*>.*<\/a>||g/g;s/^\([a-zA-Z0-9]*\)\.\(.*\)\[.*\]\[.*\]*/s|<\1.*class=.?\2.*>.*<\/\1>||g/g;s/^\([a-zA-Z0-9]*\)#\(.*\):.*[:[^:]]*[^:]*/s|<\1.*id=.?\2.*>.*<\/\1>||g/g;s/^\([a-zA-Z0-9]*\)#\(.*\)/s|<\1.*id=.?\2.*>.*<\/\1>||g/g;s/^\[\([a-zA-Z]*\).=\(.*\)\]/s|\1^=\2>||g/g;s/\^/[\/\&:\?=_]/g;s/\.\([a-zA-Z0-9]\)/\\.\1/g"" """ +
                        tmp + "\"";
                    proc = Process.Start(sed);
                    using (var reader = proc.StandardOutput)
                    {
                        var lines = reader.ReadToEnd();
                        filters.WriteLine("FILTER: {0} Tag filter of {0}", fileref);
                        filters.WriteLine(lines.Trim());
                    }
                    actions.WriteLine("{{ +filter{{{0}}} }}", fileref);
                    debug("Writing whitelist");
                    sed.Arguments = @"""/^@@.*/!d;s/^@@//g;/\$.*/d;/#/d;s/\./\\./g;s/\?/\\?/g;s/\*/.*/g;s/(/\\(/g;s/)/\\)/g;s/\[/\\[/g;s/\]/\\]/g;s/\^/[\/\&:\?=_]/g;s/^||/\./g;s/^|/^/g;s/|$/\$/g;/|/d"" """ + tmp + "\"";
                    proc = Process.Start(sed);
                    using (var reader = proc.StandardOutput)
                    {
                        var lines = reader.ReadToEnd();
                        actions.WriteLine("{ -block }");
                        actions.WriteLine(lines.Trim());
                    }
                    sed.Arguments =
                        @"""/^@@.*/!d;s/^@@//g;/\$.*image.*/!d;s/\$.*image.*//g;/#/d;s/\./\\./g;s/\?/\\?/g;s/\*/.*/g;s/(/\\(/g;s/)/\\)/g;s/\[/\\[/g;s/\]/\\]/g;s/\^/[\/\&:\?=_]/g;s/^||/\./g;s/^|/^/g;s/|$/\$/g;/|/d"" """ + tmp +
                        "\"";
                    proc = Process.Start(sed);
                    using (var reader = proc.StandardOutput)
                    {
                        var lines = reader.ReadToEnd();
                        actions.WriteLine("{ -block +handle-as-image }");
                        actions.WriteLine(lines.Trim());
                    }
                }
            }

            if (reload)
            {
                var procs = Process.GetProcessesByName("Privoxy");
                if (procs.Length > 1)
                {
                    Console.Error.WriteLine("Multiple privoxy instances are running. I wouldn't restart privoxy!");
                }
                else
                {
                    if (procs.Length == 1)
                        procs[0].Kill();
                    var ps = new ProcessStartInfo(Path.Combine(configPath, "privoxy.exe"));
                    ps.WorkingDirectory = configPath;
                    ps.WindowStyle = ProcessWindowStyle.Minimized;
                    Process.Start(ps);
                }
            }
            return 0;
        }
        static void Help(string text, string arg = null)
        {
            if (!string.IsNullOrEmpty(text))
                Console.Error.WriteLine(text);
            if (!string.IsNullOrEmpty(text))
                Console.Error.WriteLine(arg);
            Console.Error.WriteLine();
            Console.Error.WriteLine(@"adblock2privoxy: converts adblock lists into privoxy useractions and userfilters.
Command line options:
-c, --config <path>              Path to directory containing privoxy's config.txt (Default: C:\Program Files (x86)\Privoxy\)
-s, --sources <url> <url>...     Source AdBlock lists URLs (Default: ruadlisk+easylist and prebake/obtrusive)
-f, --file <basename>            Base name for filter and actions file (Default: adblock)
-x, --fix                        Fix config.txt to point to generated files (Default: off)
-r, --reload                     Restart privoxy when finished (Default: off)
-v, --verbose                    Be more verbose (Default: off)
-h, --help                       Show this info screen and exit

Exit statuses:
0  all requested operations completed successfully
-1 error parsing command line
-2 error reading/writing config.txt");
        }
    }
}
